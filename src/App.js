import React, { Component } from 'react';
import { createStore } from 'redux';
import {stacklineApp } from './state/stacklineApp';
import HeaderComponent from './components/header';
import LinksComponent from './components/links';
import InfoComponent from './components/info';
import TagsComponent from './components/tags';
import SalesGraphComponent from './components/sales_graph';
import SalesTableComponent from './components/sales_table';
import OverviewDetailComponent from './components/overview_detail';
import OverviewReviewsComponent from './components/overview_reviews';
import { 
  sortByDate, 
  sortByDateDesc, 
  sortByRetailSales, 
  sortByRetailSalesDesc,
  sortByWholesaleSales, 
  sortByUnitsSold, 
  sortByRetailerMargin, 
  sortByWholesaleSalesDesc,
  sortByUnitsSoldDesc,
  sortByRetailerMarginDesc,
  salesLinkClicked,
  overviewLinkClicked} from './state/actions'
import {
  WEEK_ENDING, RETAIL_SALES, WHOLESALE_SALES, UNITS_SOLD, RETAILER_MARGIN
} from './state/fieldNames';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.headerClickHandler = this.headerClickHandler.bind(this);
    this.linkClickHandler = this.linkClickHandler.bind(this);
    this.getData = this.getData.bind(this);
    const data = {data: this.getData()[0]};
    this.store = createStore(stacklineApp, data);

    this.unsubscribe = this.store.subscribe(() => {
      this.setState(this.store.getState());
    });

    this.state = this.store.getState();
  }

  getData() {
    return [{
      "id": "B007TIE0GQ",
      "title": "Shark Ninja",
      "image": "https://images-na.ssl-images-amazon.com/images/I/51h-a5IaHeL.jpg",
      "subtitle": "Magic Bullet NutriBullet 12-Piece High-Speed Blender/Mixer System",
      "brand": "Nutribullet",
      "reviews" : [
        {
        "customer" : "ILoveToReview",
        "review":"I had not seen the infomercial when I purchased this. I was looking for a powerful smoothie maker after my smoothie blender died. The base is MUCH heavier/substantial and well made than the Magic Bullet. It also looks more expensive/nice. I know this because I was at Target examining them both closely. The 600 watts of power has been great to tear through kale, chard, frozen berries, almonds, chia seeds. I love the consistency of the smoothie when I am done. it is not a complete liquid, but really well blended. I love that you keep the nutrition of the pulp. It is not like my first old juicer that left the pulp out of the juice, but also the nutrition. It was a NIGHTMARE to clean. Then I bought the smoothie blender that is now kaput! The Nutri Bullet literally rinses clean in ten seconds. Obviously rinse it right after you are done using it. My daughter won't touch anything green and we put kale, chard, broccoli and fruit,berries to make it NOT green and she likes it. Honestly we are NOT a family that likes kale, chard, broccoli, prunes, but feel so much better now that we are ingesting them. It took about two weeks for me to notice a difference and feel better. My husband and I have been having two a day and I have lost 4 lbs in 18 days without trying. They just keep you full and satisfied. It has taken the edge off my sweet tooth. They will keep you regular.I don\'t know why they don\'t just give you two large smoothie cups. We only use the big size.",
        "score": 5
        }
      ],
      "retailer": "Amazon",
      "details": ["Effortlessly pulverizes fruits, vegetables, superfoods and protein shakes",
        "High-torque power base and 600-watt motor",
        "Power, patented blade design with cyclonic action",
        "Includes a power base, 1 tall cup, 2 short cups, 1 flat blade and 1 emulsifying blade, 2 re-sealable lids, pocket nutritionist and manual with recipes",
        "This product is manufactured in compliance with US & Canadian Electrical Standards"
      ],
      "tags": ["Pantry", "Obsolete", "Blender", "Lightning Deal"],
      "sales": [{
        "weekEnding": "2016-01-01",
        "retailSales": 348123,
        "wholesaleSales": 255721,
        "unitsSold": 887,
        "retailerMargin": 123294
      }, {
        "weekEnding": "2016-01-08",
        "retailSales": 256908,
        "wholesaleSales": 189678,
        "unitsSold": 558,
        "retailerMargin": 67230
      }, {
        "weekEnding": "2016-01-15",
        "retailSales": 243522,
        "wholesaleSales": 255721,
        "unitsSold": 623,
        "retailerMargin": 567845
      }, {
        "weekEnding": "2016-01-22",
        "retailSales": 458646,
        "wholesaleSales": 356475,
        "unitsSold": 745,
        "retailerMargin": 453645
      }, {
        "weekEnding": "2016-01-29",
        "retailSales": 929844,
        "wholesaleSales": 405784,
        "unitsSold": 832,
        "retailerMargin": 676577
      }, {
        "weekEnding": "2016-02-05",
        "retailSales": 243522,
        "wholesaleSales": 255721,
        "unitsSold": 900,
        "retailerMargin": 978654
      }, {
        "weekEnding": "2016-02-12",
        "retailSales": 243522,
        "wholesaleSales": 255721,
        "unitsSold": 432,
        "retailerMargin": 342467
      }, {
        "weekEnding": "2016-02-19",
        "retailSales": 243522,
        "wholesaleSales": 255721,
        "unitsSold": 789,
        "retailerMargin": 734523
      }, {
        "weekEnding": "2016-02-26",
        "retailSales": 243522,
        "wholesaleSales": 255721,
        "unitsSold": 826,
        "retailerMargin": 134573
      }, {
        "weekEnding": "2016-03-05",
        "retailSales": 243522,
        "wholesaleSales": 255721,
        "unitsSold": 904,
        "retailerMargin": 423422
      }, {
        "weekEnding": "01-02-31",
        "retailSales": 243522,
        "wholesaleSales": 255721,
        "unitsSold": 508,
        "retailerMargin": 123466
      }]
    }];
  }

  headerClickHandler(sortField) {
    const state = this.store.getState();
    let sortKey = state.sortKey;
    let orderIsAscending = state.orderIsAscending;
    switch(sortField) {
      case WEEK_ENDING:
        if (sortKey !== WEEK_ENDING || !orderIsAscending) {
          this.store.dispatch(sortByDate());
        } else {
          this.store.dispatch(sortByDateDesc());
        }
        break;
      case RETAIL_SALES:
        if (sortKey !== RETAIL_SALES || !orderIsAscending) {
          this.store.dispatch(sortByRetailSales());
        } else {
          this.store.dispatch(sortByRetailSalesDesc());
        }
        break;
      case WHOLESALE_SALES:
        if (sortKey !== WHOLESALE_SALES || !orderIsAscending) {
          this.store.dispatch(sortByWholesaleSales());
        } else {
          this.store.dispatch(sortByWholesaleSalesDesc());
        }
        break;
      case UNITS_SOLD:
        if (sortKey !== UNITS_SOLD || !orderIsAscending) {
          this.store.dispatch(sortByUnitsSold());
        } else {
          this.store.dispatch(sortByUnitsSoldDesc());
        }
        break;
      case RETAILER_MARGIN:
        if (sortKey !== RETAILER_MARGIN || !orderIsAscending) {
          this.store.dispatch(sortByRetailerMargin());
        } else {
          this.store.dispatch(sortByRetailerMarginDesc());
        }
        break;
    }
  }

  linkClickHandler(isSales) {
    if (isSales) {
      this.store.dispatch(salesLinkClicked());
    } else {
      this.store.dispatch(overviewLinkClicked());
    }
  }

  render() {
    const { data, sortKey, orderIsAscending, showOverview } = this.state;
    return (
      <div className="App">
        <HeaderComponent />
        <div className="Main">
          <section className="product-info">
            <InfoComponent data={data} />
            <TagsComponent tags={data.tags} />
            <LinksComponent showOverview={showOverview} linkClickHandler={this.linkClickHandler} />
          </section>
          {
            !showOverview && 
            <section className="sales">
              <SalesGraphComponent sales={data.sales} />
              <SalesTableComponent headerClickHandler={this.headerClickHandler} sales={data.sales} sortKey={sortKey} orderIsAscending={orderIsAscending} />
            </section>
          }
          {
            showOverview && 
            <section className="sales">
              <OverviewDetailComponent details={data.details} />
              <OverviewReviewsComponent reviews={data.reviews} />
            </section>
          }
        </div>
      </div>
    );
  }
}

export default App;
