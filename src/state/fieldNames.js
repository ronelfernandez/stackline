export const WEEK_ENDING = 'weekEnding';
export const RETAIL_SALES = 'retailSales';
export const WHOLESALE_SALES = 'wholesaleSales';
export const UNITS_SOLD = 'unitsSold';
export const RETAILER_MARGIN = 'retailerMargin';
