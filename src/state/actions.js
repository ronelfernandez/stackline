export const SORT_BY_DATE = 'SORT_BY_DATE';
export const SORT_BY_DATE_DESC = 'SORT_BY_DATE_DESC';
export const SORT_BY_RETAIL_SALES = 'SORT_BY_RETAIL_SALES';
export const SORT_BY_RETAIL_SALES_DESC = 'SORT_BY_RETAIL_SALES_DESC';
export const SORT_BY_WHOLESALE_SALES = 'SORT_BY_WHOLESALE_SALES';
export const SORT_BY_WHOLESALE_SALES_DESC = 'SORT_BY_WHOLESALE_SALES_DESC';
export const SORT_BY_UNITS_SOLD = 'SORT_BY_UNITS_SOLD';
export const SORT_BY_UNITS_SOLD_DESC = 'SORT_BY_UNITS_SOLD_DESC';
export const SORT_BY_RETAILER_MARGIN = 'SORT_BY_RETAILER_MARGIN';
export const SORT_BY_RETAILER_MARGIN_DESC = 'SORT_BY_RETAILER_MARGIN_DESC';
export const SALES_LINK_CLICKED = 'SALES_LINK_CLICKED';
export const OVERVIEW_LINK_CLICKED = 'OVERVIEW_LINK_CLICKED';
/*
 * action creators
 */

export function sortByDate() {
  return { type: SORT_BY_DATE};
}

export function sortByDateDesc() {
  return { type: SORT_BY_DATE_DESC};
}

export function sortByRetailSales() {
  return { type: SORT_BY_RETAIL_SALES};
}

export function sortByRetailSalesDesc() {
  return { type: SORT_BY_RETAIL_SALES_DESC};
}

export function sortByWholesaleSales() {
  return { type: SORT_BY_WHOLESALE_SALES};
}

export function sortByWholesaleSalesDesc() {
  return { type: SORT_BY_WHOLESALE_SALES_DESC};
}

export function sortByUnitsSold() {
  return { type: SORT_BY_UNITS_SOLD};
}

export function sortByUnitsSoldDesc() {
  return { type: SORT_BY_UNITS_SOLD_DESC};
}

export function sortByRetailerMargin() {
  return { type: SORT_BY_RETAILER_MARGIN};
}

export function sortByRetailerMarginDesc() {
  return { type: SORT_BY_RETAILER_MARGIN_DESC};
}

export function salesLinkClicked() {
  return { type: SALES_LINK_CLICKED};
}

export function overviewLinkClicked() {
  return { type: OVERVIEW_LINK_CLICKED};
}