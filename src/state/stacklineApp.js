import { 
  SORT_BY_DATE,
  SORT_BY_DATE_DESC,
  SORT_BY_RETAIL_SALES,
  SORT_BY_RETAIL_SALES_DESC,
  SORT_BY_WHOLESALE_SALES, 
  SORT_BY_WHOLESALE_SALES_DESC,
  SORT_BY_UNITS_SOLD,
  SORT_BY_RETAILER_MARGIN, 
  SORT_BY_UNITS_SOLD_DESC,
  SORT_BY_RETAILER_MARGIN_DESC,
  SALES_LINK_CLICKED,
  OVERVIEW_LINK_CLICKED} from './actions';
import { WEEK_ENDING } from './fieldNames';
export function stacklineApp(state = { sortKey: WEEK_ENDING, orderIsAscending: true, showOverview: false }, action) {
  let sales = state.data.sales || [];
  let sortKey = state.sortKey;
  let orderIsAscending = state.orderIsAscending;
  let showOverview = state.showOverview;
  switch (action.type) {
    case SORT_BY_DATE:
      sortKey = 'weekEnding';
      orderIsAscending = true;
      break;
    case SORT_BY_DATE_DESC:
      sortKey = 'weekEnding';
      orderIsAscending = false;
      break;
    case SORT_BY_RETAIL_SALES:
      orderIsAscending = true;
      sortKey = 'retailSales';
      break;
    case SORT_BY_RETAIL_SALES_DESC:
      sortKey = 'retailSales';
      orderIsAscending = false;
    break;
      case SORT_BY_RETAIL_SALES:
      orderIsAscending = true;
      sortKey = 'retailSales';
      break;
    case SORT_BY_RETAIL_SALES_DESC:
      sortKey = 'retailSales';
      orderIsAscending = false;
      break;
      case SORT_BY_WHOLESALE_SALES:
      orderIsAscending = true;
      sortKey = 'wholesaleSales';
      break;
    case SORT_BY_WHOLESALE_SALES_DESC:
      sortKey = 'wholesaleSales';
      orderIsAscending = false;
      break;
    case SORT_BY_WHOLESALE_SALES:
      orderIsAscending = true;
      sortKey = 'wholesaleSales';
      break;
    case SORT_BY_WHOLESALE_SALES_DESC:
      sortKey = 'wholesaleSales';
      orderIsAscending = false;
      break;
    case SORT_BY_UNITS_SOLD:
      orderIsAscending = true;
      sortKey = 'unitsSold';
      break;
    case SORT_BY_UNITS_SOLD_DESC:
      orderIsAscending = false;
      sortKey = 'unitsSold';
      break;
    case SORT_BY_RETAILER_MARGIN:
      sortKey = 'retailerMargin';
      orderIsAscending = true;
      break;
    case SORT_BY_RETAILER_MARGIN_DESC:
      sortKey = 'retailerMargin';
      orderIsAscending = false;
      break;
    case SALES_LINK_CLICKED:
      showOverview = true;
      break;
    case OVERVIEW_LINK_CLICKED:
      showOverview = false;
      break;
  }
  sales = sales.sort((a, b) => {
    if (a[sortKey] < b[sortKey]) {
      return orderIsAscending ? -1 : 1;
    }
    if (a[sortKey] > b[sortKey]) {
      return orderIsAscending ? 1 : -1;
    }
    // a must be equal to b
    return 0;
  });
  const data = Object.assign({}, state.data, { sales });
  return Object.assign({}, state, { data, sortKey, orderIsAscending, showOverview });
}
