import React, { Component } from 'react';

class TagsComponent extends Component {
  render() {
    const { tags } = this.props;
    return (
      <section className="Tags">
        <ul>
          {
            tags.map((tag) => {
              return <li>{tag}</li>;
            })
          }
          </ul>
      </section>
    );
  }
}

export default TagsComponent;
