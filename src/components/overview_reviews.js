import React, { Component } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend  } from 'recharts';

class OverviewReviewsComponent extends Component {
  render() {
    const { reviews } = this.props;
    return (
      <div className="sales-graph reviews">
        <ul>
        {
          reviews.map((review) => {
            return (
              <li>
                <span className="reviewer"><b>{review.customer}</b>&nbsp;{review.score} stars</span>
                <span>{review.review}</span>
              </li>
            )
          })
        }
        </ul>
      </div>
    );
  }
}

export default OverviewReviewsComponent;
