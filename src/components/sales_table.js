import React, { Component } from 'react';
import {
  WEEK_ENDING, RETAIL_SALES, WHOLESALE_SALES, UNITS_SOLD, RETAILER_MARGIN
} from '../state/fieldNames';
import DownArrowComponent from './down_arrow';
import UpArrowComponent from './up_arrow';

class SalesTableComponent extends Component {
  render() {
    const { sales, headerClickHandler, sortKey, orderIsAscending } = this.props;
    return (
      <div className="sales-table">
        <table>
          <thead>
            <tr>
              <th onClick={() => headerClickHandler(WEEK_ENDING)} className="week-ending">
                <div>
                WEEK ENDING
                {
                  sortKey === WEEK_ENDING && orderIsAscending &&
                  <UpArrowComponent />
                }
                {
                  sortKey === WEEK_ENDING && !orderIsAscending &&
                  <DownArrowComponent />
                }
                </div>
              </th>
              <th onClick={() => headerClickHandler(RETAIL_SALES)} className="retail-sales">
                <div>
                  RETAIL SALES
                  {
                    sortKey === RETAIL_SALES && orderIsAscending &&
                    <UpArrowComponent />
                  }
                  {
                    sortKey === RETAIL_SALES && !orderIsAscending &&
                    <DownArrowComponent />
                  }
                </div>
              </th>
              <th onClick={() => headerClickHandler(WHOLESALE_SALES)} className="wholesale-sales">
                <div>
                  WHOLESALE SALES
                  {
                    sortKey === WHOLESALE_SALES && orderIsAscending &&
                    <UpArrowComponent />
                  }
                  {
                    sortKey === WHOLESALE_SALES && !orderIsAscending &&
                    <DownArrowComponent />
                  }
                </div>
              </th>
              <th onClick={() => headerClickHandler(UNITS_SOLD)} className="units-sold">
                <div>
                  UNITS SOLD
                  {
                    sortKey === UNITS_SOLD && orderIsAscending &&
                    <UpArrowComponent />
                  }
                  {
                    sortKey === UNITS_SOLD && !orderIsAscending &&
                    <DownArrowComponent />
                  }
                </div>
              </th>
              <th onClick={() => headerClickHandler(RETAILER_MARGIN)} className="retailer-margin">
                <div>
                  RETAILER MARGIN
                  {
                    sortKey === RETAILER_MARGIN && orderIsAscending &&
                    <UpArrowComponent />
                  }
                  {
                    sortKey === RETAILER_MARGIN && !orderIsAscending &&
                    <DownArrowComponent />
                  }
                </div>
              </th>
            </tr>  
          </thead>
          <tbody>
          {
            sales.map ((sale) => {
              return (
                <tr>
                  <td className="week-ending">
                    {sale.weekEnding}
                  </td>
                  <td className="retail-sales">
                    {new Intl.NumberFormat('en-US', { 
                      style: 'currency', 
                      currency: 'USD',
                      minimumFractionDigits: 0, 
                      maximumFractionDigits: 0 
                    }).format(sale.retailSales)}                    
                  </td>
                  <td className="wholesale-sales">
                    {new Intl.NumberFormat('en-US', { 
                      style: 'currency', 
                      currency: 'USD',
                      minimumFractionDigits: 0, 
                      maximumFractionDigits: 0 
                    }).format(sale.wholesaleSales)}                    
                  </td>
                  <td className="units-sold">{sale.unitsSold}</td>
                  <td className="retailer-margin">
                    {new Intl.NumberFormat('en-US', { 
                      style: 'currency', 
                      currency: 'USD',
                      minimumFractionDigits: 0, 
                      maximumFractionDigits: 0 
                    }).format(sale.retailerMargin)}                    
                  </td>
                </tr>
              );
            })
          }
          </tbody>
        </table>
      </div>
    );
  }
}

export default SalesTableComponent;
