import React, { Component } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend  } from 'recharts';

class OverviewDetailComponent extends Component {
  render() {
    const { details } = this.props;
    return (
      <div className="details">
        <h1>Details</h1>
        {details}
      </div>
    );
  }
}

export default OverviewDetailComponent;
