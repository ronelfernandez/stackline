import React, { Component } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend  } from 'recharts';

class SalesGraphComponent extends Component {
  constructor(props) {
    super(props);

  }
  render() {
    const { sales } = this.props;
    return (
      <div className="sales-graph">
        <h1>Retail Sales</h1>

        <LineChart width={1000} height={400} data={sales}
              margin={{top: 5, right: 30, left: 20, bottom: 5}}>
        <XAxis dataKey="weekEnding"/>
        <YAxis/>
        <CartesianGrid strokeDasharray="3 3"/>
        <Tooltip/>
        <Legend />
        <Line type="monotone" dataKey="retailSales" stroke="#8884d8" activeDot={{r: 8}}/>
        <Line type="monotone" dataKey="wholesaleSales" stroke="#82ca9d" />
        </LineChart>
      </div>
    );
  }
}

export default SalesGraphComponent;
