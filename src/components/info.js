import React, { Component } from 'react';

class InfoComponent extends Component {
  constructor(props) {
    super(props);

  }
  render() {
    const { title, image, subtitle } = this.props.data;
    return (
      <div className="Info">
          <img className="product-logo" src={image} />
          <h1 className="product-title">{title}</h1>
          <h2 className="product-subtitle">{subtitle}!</h2>
      </div>
    );
  }
}

export default InfoComponent;
